const mongoConnectionString = 'mongodb://0.0.0.0/agenda';
const Agenda = require('agenda')

const agenda = new Agenda({db: {address: mongoConnectionString, useNewUrlParser: true}});

agenda.define("average_conn_time", function () {
    console.log('average')
});

agenda.define("haha", function () {
    console.log('average')
});

(async () => {
    await agenda.start()

    await agenda.every("30 seconds", "average_conn_time")
    await agenda.every("1 minutes", "haha")
})();

