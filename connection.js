const Sequelize = require('sequelize');
const cacher = require('sequelize-redis-cache');
const redis = require('redis')

const OPENWIFI_CONFIG = {
    database: "openwifi",
    username: "root",
    password: "123456",
    host: "localhost"
}

const RADIUS_CONFIG = {
    database: "radius",
    username: "root",
    password: "123456",
    host: "localhost"
}

const OpenWifi = new Sequelize(OPENWIFI_CONFIG.database, OPENWIFI_CONFIG.username, OPENWIFI_CONFIG.password, {
    host: OPENWIFI_CONFIG.host,
    dialect: 'mysql',
    logging: false,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

    operatorsAliases: false
});

const Radius = new Sequelize(RADIUS_CONFIG.database, RADIUS_CONFIG.username, RADIUS_CONFIG.password, {
    host: RADIUS_CONFIG.host,
    dialect: 'mysql',
    logging: false,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

    operatorsAliases: false
});

var rc = redis.createClient(6379, 'localhost');

// var OPENWIFI = cacher(OpenWifi, rc).ttl(null);
// var RADIUS = cacher(Radius, rc).ttl(null);
var OPENWIFI = OpenWifi,
RADIUS = Radius

module.exports = { OPENWIFI, RADIUS }