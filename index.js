const express = require('express')
const app = express();
const cors = require('cors')

app.use(cors())


//Dashboard Controllers
const DashboardController = require('./controllers/DashboardController')
    app.get('/api/AverageConnTime', DashboardController.getAverageConnTime)
    app.get('/api/ConnectionDashboard', DashboardController.getConnectionDashboard)
    app.get('/api/ImpressionDashboard', DashboardController.getImpressionsDashboard)
    app.get('/api/UserOnlineDashboard', DashboardController.getUserOnlineDashboard)
    app.get('/api/AverageTraffic', DashboardController.getAverageTraffic)

    app.get('/api/data_connection', DashboardController.getDataConnection)
    app.get('/api/AvgTime', DashboardController.getAvgTime)



app.listen(3000, () => console.log('Example app listening on port 3000!'))
