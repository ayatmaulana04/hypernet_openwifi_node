const { OPENWIFI, RADIUS } = require('../connection')
const Query = require('../helpers/query')
const Moment = require('../helpers/date')
const ReadableBytes = require('../helpers/convertion')

const { DATABASE } = require('../constant.js')

const {
    getAverageConnTime,
    getConnectionDashboard,
    getUserOnlineDashboard,
    getAverageTraffic,
    getImpressionsDashboard,

    getDataConnection,
    getAvgTime
} = require('../repositories/DashboardRepository')


const cronImpressionsDashboard = async () => {
    try {
        var data = await getImpressionsDashboard()
        var total = data[0].Total
        var query = `UPDATE mtz_dashboard SET date = NOW(), total = ${total} WHERE name = "impression_dashboard"`
        var hit = await RADIUS.query(query)

        console.log(`cron ImpressionDashboard ---> success (${total})`)
    } catch (e) {
        throw new Error(e)
        return false
    }
}

const cronAverageConnTime = async () => {
    try {
        var data = await getAverageConnTime()
        var total = data[0].Total
        var query = `UPDATE mtz_dashboard SET date = NOW(), total = ${total} WHERE name = "average_conn_time"`
        var hit = await RADIUS.query(query)

        console.log(`cron averageConnTime ---> success (${total})`)
    } catch (e) {
        throw new Error(e)
        return false
    }
}

const cronConnectionDashboard = async () => {
    try {
        var data = await getConnectionDashboard()
        var total = data[0].Total
        var query = `UPDATE mtz_dashboard SET date = NOW(), total = ${total} WHERE name = "connection_dashboard"`
        var hit = await RADIUS.query(query)
        console.log(`cron ConnectionDashboard ---> success (${total})`)
    } catch (e) {
        throw new Error(e)
        return false
    }
}

const cronUserOnlineDashboard = async () => {
    try {
        var data = await getUserOnlineDashboard()
        var total = data[0].Total
        var query = `UPDATE mtz_dashboard SET date = NOW(), total = ${total} WHERE name = "user_online_dashboard"`
        var hit = await RADIUS.query(query)
        console.log(`cron UserOnlineDashboard ---> success (${total})`)
    } catch (e) {
        throw new Error(e)
        return false
    }
}

const cronAverageTraffic = async () => {
    try {
        var data = await getAverageTraffic()
        var total = data
        var query = `UPDATE mtz_dashboard SET date = NOW(), total = ${total} WHERE name = "average_traffic"`
        var hit = await RADIUS.query(query)
        console.log(`cron AveragTraffic ---> success (${total})`)
    } catch (e) {
        throw new Error(e)
        return false
    }
}


//chart
const cronDataConnection = async () => {
    try {
        var data = await getDataConnection()
        data = JSON.stringify(data)
        var query = `UPDATE mtz_dashboard SET date = NOW(), json = '${data}' WHERE name = "data_connection"`
        var hit = await RADIUS.query(query)
        console.log(`cron DataConnection ---> success (${data})`)
    } catch (e) {
        throw new Error(e)
        return false
    }
}

const cronAvgTime = async () => {
    try {
        var data = await getAvgTime()
        data = JSON.stringify(data)
        var query = `UPDATE mtz_dashboard SET date = NOW(), json = '${data}' WHERE name = "avg_time"`
        var hit = await RADIUS.query(query)
        console.log(`cron AvgTime ---> success (${data})`)
    } catch (e) {
        throw new Error(e)
        return false
    }
}

module.exports = {
    cronImpressionsDashboard,
    cronAvgTime,
    cronAverageTraffic,
    cronDataConnection,
    cronAverageConnTime,
    cronConnectionDashboard,
    cronUserOnlineDashboard,
}

// (async () => {
//     const arguments = process.argv.slice(2)
//     // ["basic", "impression", "avgtime", "dataconnection]
//
//     try {
//         if( arguments[0] == "basic" ) {
//             await cronAverageConnTime()
//             await cronConnectionDashboard()
//             await cronUserOnlineDashboard()
//             await cronAverageTraffic()
//         }
//
//         if( arguments[0] == "impression" )
//             await cronImpressionsDashboard()
//
//         if( arguments[0] == "avgtime")
//             await cronDataConnection()
//
//         if( arguments[0] == "dataconnection")
//             await cronAvgTime()
//
//         console.log("succes")
//     } catch (e) {
//         throw new Error(e)
//     } finally {
//         process.exit(1)
//     }
//
// })()
