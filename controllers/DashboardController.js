const { OPENWIFI, RADIUS } = require('../connection')
const Query = require('../helpers/query')
const Moment = require('../helpers/date')
const ReadableBytes = require('../helpers/convertion')

const { DATABASE } = require('../constant.js')

const DashboardRepository = require('../repositories/DashboardRepository.js')

const querySelect= "SELECT total, json FROM mtz_dashboard WHERE name = "

const getAverageConnTime = async (req, res) => {
    try {
       var data = await Query(DATABASE.RADIUS, querySelect + `"average_conn_time"`)
    } catch(e) {
        return res.send("error")
        throw new Error(e)
    }

    res.send(data[0].total.toString())
}

const getConnectionDashboard = async (req, res) => {
    try {
        var data = await Query(DATABASE.RADIUS, querySelect + `"connection_dashboard"`)
    } catch(e) {
        return res.send("error")
        throw new Error(e)
    }

    res.send(data[0].total.toString())
}

const getImpressionsDashboard = async (req, res) => {
    var query = `SELECT COUNT(*) as Total FROM msdailyimpressions WHERE RegisterDate = '${Moment.todayDate}'`
    var replacements = [Moment.todayDate]

    try {
        var data = await Query(DATABASE.OPENWIFI, query, replacements)
    } catch(e) {
        throw new Error(e)
    }

    res.send(data[0].Total.toString())
}
const getUserOnlineDashboard = async (req, res) => {
    try {
        var data = await Query(DATABASE.RADIUS, querySelect + `"user_online_dashboard"`)
    } catch(e) {
        return res.send("error")
        throw new Error(e)
    }

    res.send(data[0].total.toString())
}

const getAverageTraffic = async (req, res) => {
    try {
        var data = await Query(DATABASE.RADIUS, querySelect + `"average_traffic"`)
    } catch(e) {
        return res.send("error")
        throw new Error(e)
    }

    res.send(data[0].total.toString())
}



const getDataConnection = async (req, res) => {
    try {
        var data = await Query(DATABASE.RADIUS, querySelect + `"data_connection"`)
    } catch(e) {
        return res.send("error")
        throw new Error(e)
    }

    res.json(JSON.parse(data[0].json))
}


const getAvgTime = async (req, res) => {
    try {
        var data = await Query(DATABASE.RADIUS, querySelect + `"avg_time"`)
    } catch(e) {
        return res.send("error")
        throw new Error(e)
    }

    res.json(JSON.parse(data[0].json))
}


module.exports = {
    getAverageConnTime,
    getImpressionsDashboard,
    getConnectionDashboard,
    getUserOnlineDashboard,
    getAverageTraffic,

    getDataConnection,
    getAvgTime

}