const { OPENWIFI, RADIUS } = require('../connection')
const Query = require('../helpers/query')
const Moment = require('../helpers/date')
const ReadableBytes = require('../helpers/convertion')

const { DATABASE } = require('../constant.js')

const getAverageConnTime = async () => {
    var query = `SELECT COUNT(DISTINCT username) AS Total FROM radacct WHERE Date(acctstarttime) = '${Moment.todayDate}'`
    var replacements = [Moment.todayDate]

    try {
        var data = await Query(DATABASE.RADIUS, query, replacements)
    } catch(e) {
        return false
        throw new Error(e)
    }

    return data
}

const getConnectionDashboard = async () => {
    var query = `SELECT COUNT(*) as Total FROM radacct WHERE Date(acctstarttime) = '${Moment.todayDate}'`
    var replacements = [Moment.todayDate]

    try {
        var data = await Query(DATABASE.RADIUS, query, replacements)
    } catch(e) {
        return false;
        throw new Error(e)
    }

    return data
}

const getImpressionsDashboard = async () => {
    var query = `SELECT COUNT(*) as Total FROM msdailyimpressions WHERE RegisterDate = '${Moment.todayDate}'`
    var replacements = [Moment.todayDate]

    try {
        var data = await Query(DATABASE.OPENWIFI, query, replacements)
    } catch(e) {
        return false
        throw new Error(e)
    }

    return data
}
const getUserOnlineDashboard = async (req, res) => {
    var query = `SELECT COUNT(*) as Total FROM radacct WHERE Date(acctstarttime) = '${Moment.todayDate}' AND acctstoptime IS NULL`
    var replacements = [Moment.todayDate]

    try {
        var data = await Query(DATABASE.RADIUS, query, replacements)
    } catch(e) {
        return false
        throw new Error(e)
    }

    return data
}

const getAverageTraffic = async (req, res) => {
    var query = `SELECT SUM(acctoutputoctets) AS Download, SUM(acctinputoctets) AS Upload FROM radacct WHERE Date(acctstarttime) = '${Moment.todayDate}'`
    var replacements = [Moment.todayDate]

    try {
        var data = await Query(DATABASE.RADIUS, query, replacements)
        if(data[0].Download && data[0].Upload) {
            data = ReadableBytes(data[0].Download + data[0].Upload)
        }
        data = "0"
    } catch(e) {
        return false
        throw new Error(e)
    }

    return data
}

const getDataConnection = async (req, res) => {
    var query = `SELECT COUNT(*) AS Total FROM radacct WHERE Date(acctstarttime) = `
    var days = Moment.sevenDayAgo()

    var result = []
    try {
        for ( let i = 0;  i <= 7; i++) {
            let data = await Query(DATABASE.RADIUS, query + days[i])
            if(data[0].Total > 0) {
                result.push(data[0].Total)
            } else {
                result.push(0)
            }
        }
    } catch(e) {
        return false
        throw new Error(e)
    }


    var responses = [days, result]
    return responses
}


const getAvgTime = async (req, res) => {
    var queryTime = `SELECT SUM(acctsessiontime) AS Total FROM radacct WHERE Date(acctstarttime) = `
    var queryUserDay = `SELECT COUNT(DISTINCT username) AS Total FROM radacct WHERE Date(acctstarttime) = `
    var days = Moment.sevenDayAgo()

    var result = []

    try{
        for ( let i = 0;  i <= 7; i++) {
            let data = await Query(DATABASE.RADIUS, queryUserDay + days[i])
            if(data[0].Total > 0) {
                let dataTime = await Query(DATABASE.RADIUS, queryTime + days[i])
                let total = dataTime[0].Total / data[0].Total
                result.push(total)
            } else {
                result.push(0)
            }
        }
    } catch(e) {
        return false
    }

    var responses = [days, result]
    return responses
}


module.exports = {
    getAverageConnTime,
    getImpressionsDashboard,
    getConnectionDashboard,
    getUserOnlineDashboard,
    getAverageTraffic,

    getDataConnection,
    getAvgTime

}