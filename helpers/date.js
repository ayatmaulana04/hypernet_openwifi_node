const moment = require('moment')

const todayDate = moment().format("YYYY-MM-DD")
const sevenDayAgo = () => {
    var days = []
    for (let i = 7; i >= 0; i-- ) {
        days.push( moment().add('-'+i, 'day').format('YYYY-MM-DD') )
    }

    return days
}

module.exports = {
    todayDate,
    sevenDayAgo
}