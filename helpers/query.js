const { OPENWIFI, RADIUS } = require('../connection')
const Sequelize = require('sequelize')

const { DATABASE } = require('../constant')

const Query = async (database, query, replacements) => {
    const DB = (database == DATABASE.OPENWIFI) ? OPENWIFI : RADIUS
    try {
        var data = await DB.query(query, {
            replacements,
            type: Sequelize.QueryTypes.SELECT
        } )
    } catch(e) {
        throw new Error(e)
    }

    return data
}

module.exports = Query