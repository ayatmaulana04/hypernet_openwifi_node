const timexe = require('timexe')

const DashboardCron = require('./crons/DashboardCron')

console.log("setuping cron ......... ")
console.log("cron begin ...")

//dashboard
timexe("*/1 * * *", DashboardCron.cronImpressionsDashboard)
timexe("*/1 * * *", DashboardCron.cronAverageConnTime)
timexe("*/1 * * *", DashboardCron.cronAvgTime)
timexe("*/1 * * *", DashboardCron.cronDataConnection)
timexe("*/1 * * *", DashboardCron.cronConnectionDashboard)
timexe("*/1 * * *", DashboardCron.cronUserOnlineDashboard)
timexe("*/1 * * *", DashboardCron.cronAverageTraffic)